# wg-relay
This module creates an EC2 to serve as relay for WireGuard traffic.

It also generate WireGuard interface configuration files for each peer.
These are stored in the root module directory, by default.
