##############################
# Module for wg_relay server
##############################


# ssh key for accessing the relay
resource "aws_key_pair" "wg_relay_key_pair" {
  key_name_prefix = "wg-server-"
  public_key      = var.ssh_pub_key

  tags = {
    Project = var.project_tag
  }
}


# the server itself
resource "aws_instance" "wg_relay" {
  ami                         = var.wg_relay_ami
  instance_type               = var.wg_relay_size
  availability_zone           = var.availability_zone
  key_name                    = aws_key_pair.wg_relay_key_pair.key_name
  monitoring                  = true
  associate_public_ip_address = true
  subnet_id                   = var.wgr_subnet_id
  vpc_security_group_ids      = var.wgr_sg_ids

  # execute ansible playbooks
  provisioner "local-exec" {
    # use current public ip becuase eip is not associated yet
    command = "ansible-playbook -u ubuntu -i '${self.public_ip},' --private-key ${var.ssh_priv_key_path} -e wg_conf_path=${var.wg_conf_path} ${path.module}/playbooks/wg_relay.yml"
  }

  tags = {
    Name    = "Wireguard Relay Server"
    Project = var.project_tag
  }
}


# associate the elastic ip with the ec2 instance
resource "aws_eip_association" "wg_relay_eip" {
  instance_id   = aws_instance.wg_relay.id
  allocation_id = var.wgr_eip_id
}
