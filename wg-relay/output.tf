#### wg_relay ####

output "wg_relay_arn" {
  value = aws_instance.wg_relay.arn
}

output "private_ip" {
  value = aws_instance.wg_relay.private_ip
}
