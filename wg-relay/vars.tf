#### wg_relay ####

variable "availability_zone" {
  type = string
}

variable "project_tag" {
  type    = string
  default = "WireGuard Relay"
}

variable "ssh_priv_key_path" {
  description = "Path to private ssh key for Ansible ssh"
  type        = string
}

variable "wg_relay_ami" {
  type    = string
  default = "ami-01cd5988241256cd8" # ubuntu-eoan-19.10-amd64-minimal-20200317
}

variable "wgr_eip_id" {
  description = "Elastic IP to associate with wg relay"
  type        = string
}

variable "ssh_pub_key" {
  description = "The contents of the public RSA key for Ansible ssh"
  type        = string
}

variable "wg_conf_path" {
  description = "The path to the wg conf file this relay should get"
  type        = string
}
variable "wg_relay_size" {
  type    = string
  default = "t3.micro"
}

variable "wgr_sg_ids" {
  description = "List of security group IDs to associate with EC2"
  type        = list(string)
}

variable "wgr_subnet_id" {
  description = "ID of AWS private subnet"
  type        = string
}