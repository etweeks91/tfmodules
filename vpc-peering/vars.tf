variable "project_tag" {
  type = string
}

variable "vpc1" {
  type = string
}

variable "vpc1_region" {
  type = string
}

variable "vpc1_cidr" {
  type = string
}

variable "vpc1_route_table" {
  type = string
}

variable "vpc2" {
  type = string
}

variable "vpc2_region" {
  type = string
}

variable "vpc2_cidr" {
  type = string
}

variable "vpc2_route_table" {
  type = string
}
