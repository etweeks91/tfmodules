provider "aws" {
  alias = "requestor"
}

provider "aws" {
  alias = "acceptor"
}

data "aws_caller_identity" "requestor_data" {
  provider = aws.requestor
}

resource "aws_vpc_peering_connection" "requestor" {
  provider      = aws.requestor
  peer_owner_id = data.aws_caller_identity.requestor_data.account_id
  vpc_id        = var.vpc1
  auto_accept   = false
  peer_vpc_id   = var.vpc2 # who you want to connect with
  peer_region   = var.vpc2_region
  tags = {
    Name = var.project_tag
  }
}

resource "aws_vpc_peering_connection_accepter" "acceptor" {
  provider                  = aws.acceptor
  vpc_peering_connection_id = aws_vpc_peering_connection.requestor.id
  auto_accept               = true

}

resource "aws_route" "vpc1_peering_update" {
  provider                  = aws.requestor
  route_table_id            = var.vpc1_route_table
  destination_cidr_block    = var.vpc2_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.requestor.id
}

resource "aws_route" "vpc2_peering_update" {
  provider                  = aws.acceptor
  route_table_id            = var.vpc2_route_table
  destination_cidr_block    = var.vpc1_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.requestor.id
}
