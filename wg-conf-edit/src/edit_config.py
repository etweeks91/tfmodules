"""
Edit the wireguard peer configuration files
"""

import argparse
from ipaddress import ip_address
import configparser
from pathlib import Path
from os import listdir
from os.path import isfile, join
import re

def main():

    # parse args
    parser = argparse.ArgumentParser(
        description='Generate WireGuard interface config files')
    parser.add_argument('relay_pvt_ip', type=str,
                        help='The public IP of the wg relay server')
    parser.add_argument('--peerdir', type=str, default='/tmp/wgr/config/peer',
                        help='Where to place wg peer conf files')

    # process args and make availale
    args = parser.parse_args()
    peerdir = Path(args.peerdir)

    # verify relay_public_ip is valid
    try:
        wgr_pvt_ip = ip_address(args.relay_pvt_ip)
    except ValueError:
        raise

    # configparser module makes config files accessible like a python dict
    config = configparser.ConfigParser()
    config.read(peerdir)
    config['Peer']['Endpoint'] = str(wgr_pvt_ip) + str(":51820")
    with open(peerdir, "w") as configfile:
        config.write(configfile)

if __name__ == "__main__":
    main()


