/***********************************************
wg-conf-edit module
************************************************/


resource "null_resource" "edit_wg_confs" {
  provisioner "local-exec" {
    command = "python ${path.module}/src/edit_config.py ${var.wgr_pvt_ip} --peerdir ${var.wgr_pconf_dir}"
  }
}
