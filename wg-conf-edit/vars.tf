# wg-confs-edit

variable "wgr_pconf_dir" {
  description = "Destination dir for wg peer conf files"
  type        = string
  default     = "/tmp/wgr/config/peers"
}

variable "wgr_pvt_ip" {
  description = "Publically addressible IP address for relay"
  type        = string
}
