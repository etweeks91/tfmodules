provider "aws" {
  alias = "entry"
}

provider "aws" {
  alias = "exit"
}

resource "aws_security_group" "entry_sg" {
  provider    = aws.entry
  name        = join(" ", [var.project_tag, "entry security group"])
  description = "security group for SSH, HTTPS, DNS"
  vpc_id      = var.entry_vpc

  ingress { # allows admin to SSH in for direct configuration/maintenance
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = var.allowed_ssh_ips
  }

  ingress { # allows users to wg in, but not anyone else (sg are stateful)
    from_port   = var.wg_port
    to_port     = var.wg_port
    protocol    = "UDP"
    cidr_blocks = var.allowed_ssh_ips
  }

  ingress { # accepts wg connection from exit
    from_port   = var.wg_port
    to_port     = var.wg_port
    protocol    = "UDP"
    cidr_blocks = [var.exit_node_subnet]
  }

  egress { # allows node to make DNS requests from Google or Cloudflare
    from_port   = 53
    to_port     = 53
    protocol    = "UDP"
    cidr_blocks = ["1.1.1.1/32", "8.8.8.8/32"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # egress { #for ping tests
  #   from_port   = 0
  #   to_port     = 0
  #   protocol    = "ICMP"
  #   cidr_blocks = var.vpc_allowed_egress_ips
  # }

  tags = {
    Project = var.project_tag
  }
}

resource "aws_security_group" "exit_sg" {
  provider    = aws.exit
  name        = join(" ", [var.project_tag, "exit security group"])
  description = "security group for SSH, HTTPS, DNS"
  vpc_id      = var.exit_vpc

  ingress { # allows admin to SSH in for direct configuration/maintenance
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = var.allowed_ssh_ips
  }

  egress { # initiates wg connections to entry subnet
    from_port   = var.wg_port
    to_port     = var.wg_port
    protocol    = "UDP"
    cidr_blocks = [var.entry_node_subnet]
  }

  egress { # allows node to make DNS requests from Google or Cloudflare
    from_port   = 53
    to_port     = 53
    protocol    = "UDP"
    cidr_blocks = ["1.1.1.1/32", "8.8.8.8/32"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # egress { #for ping tests
  #   from_port   = 0
  #   to_port     = 0
  #   protocol    = "ICMP"
  #   cidr_blocks = var.vpc_allowed_egress_ips
  # }

  tags = {
    Project = var.project_tag
  }
}
