variable "entry_node_subnet" {
  type = string
}

variable "exit_node_subnet" {
  type = string
}

variable "entry_vpc" {
  type = string
}

variable "exit_vpc" {
  type = string
}

variable "wg_port" {
  type = number
}

variable "allowed_ssh_ips" {
  type = list(string)
}

variable "project_tag" {
  type = string
}
