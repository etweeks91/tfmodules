###### OUTPUTS ENTRY_EXIT_SG ######

output "entry_sg_id" {
  value       = aws_security_group.entry_sg.id
  description = "The ID of the entry security group"
}

output "exit_sg_id" {
  value       = aws_security_group.exit_sg.id
  description = "The ID of the exit security group"
}