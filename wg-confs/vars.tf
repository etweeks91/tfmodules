# wg-confs

variable "num_wg_peers" {
  description = "Total number of wg peers to create"
  type        = number
}

variable "wgr_pconf_dir" {
  description = "Destination dir for wg peer conf files"
  type        = string
  default     = "/tmp/wgr/config/peers"
}

variable "wgr_rconf_dir" {
  description = "Destination dir for wg relay conf file"
  type        = string
  default     = "/tmp/wgr/config/relay"
}

variable "wgr_listen_port" {
  description = "UDP port wg relay is listening on"
  type        = number
}

variable "wgr_public_ip" {
  description = "Publically addressible IP address for relay"
  type        = string
}

variable "wgr_vpn_subnet" {
  description = "Private subnet of wg VPN in CIDR notation"
  type        = string
}
