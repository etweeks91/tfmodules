# wg-confs
Create WireGuard Configuration files with a given public endpoint

*inputs:*
*  `wg_conf_dir`: destination for conf files
*  `wgr_listen_port`: the linstening port
*  `wgr_public_ip`: the public ip of the wg relay
*  `wgr_vpn_subnet`: the private ip address for wg VPN
*  `num_wg_peers`: how many peer files to generate

*outputs*
files to `config/`
