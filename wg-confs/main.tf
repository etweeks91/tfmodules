/***********************************************
wg-confs module
************************************************/


resource "null_resource" "generate_wg_confs" {
  provisioner "local-exec" {
    command = "python ${path.module}/src/make_configs.py ${var.wgr_public_ip} ${var.wgr_listen_port} --subnet ${var.wgr_vpn_subnet} --peers ${var.num_wg_peers} --wgrdir ${var.wgr_rconf_dir} --peerdir ${var.wgr_pconf_dir}"
  }
}
