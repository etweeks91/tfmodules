#********************************
# Example for vpc-peering
#*********************************

provider "aws" {
  region = "us-east-1"
  alias  = "vpc_aws_example"
}

module "vpc_gateway" {
  source = "../../vpc-gateway/"

  providers = {
    aws = aws.vpc_aws_example
  }

  vpc1             = example_vpc1
  vpc1_region      = "us-east-1"
  vpc1_cidr        = "172.45.23.0/24"
  vpc1_route_table = vpc1_route_table
  vpc2             = example_vpc2
  vpc2_region      = "us-east-1"
  vpc2_cidr        = "177.45.23.0/24"
  vpc2_route_table = vpc2_route_table
  project_tag      = example_project
}