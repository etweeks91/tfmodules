#********************************
# Example for vpc-gateway
#*********************************


provider "aws" {
  region = "us-east-1"
  alias  = "wgr_aws_example"
}

module "wg_relay" {
  source = "../../wg-relay/"

  providers = {
    aws = aws.wgr_aws_example
  }

  availability_zone = "us-east-1a"
  ssh_priv_key_path = "/tmp/key"
  wgr_eip_id        = 123456
  ssh_pub_key       = "aaaa"
  wgr_sg_ids        = [12345]
  wgr_subnet_id     = 789
  project_tag       = "wg-relay example"
  wg_conf_path      = "path"
}
