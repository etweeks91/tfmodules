#********************************
# Example for vpc-conf
#*********************************

module "wg_confs" {
  source = "../../wg-confs"

  wgr_listen_port = 8745
  wgr_public_ip   = "45.23.56.1"
  wgr_vpn_subnet  = "10.10.10.0/24"
  num_wg_peers    = 2
  wgr_rconf_dir   = "/tmp/wgr/config/relay"
  wgr_pconf_dir   = "/tmp/wgr/config/peers"
}