#********************************
# Example for vpc-gateway
#*********************************

provider "aws" {
  region = "us-east-1"
  alias  = "vpc_aws_example"
}

module "vpc_gateway" {
  source = "../../vpc-gateway/"

  providers = {
    aws = aws.vpc_aws_example
  }
  project_tag = example_project
}