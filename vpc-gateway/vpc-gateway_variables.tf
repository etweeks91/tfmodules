
variable "vpc_subnet_cidr" {
  default     = "192.168.1.0/24"
  type        = string
  description = "The VPC subnet range with cidr"
}

variable "subnet_az" {
  type        = string
  description = "The availability zone for the subnet"
}

variable "project_tag" {
  default     = ""
  description = "Name of project"
}