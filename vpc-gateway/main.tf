### stitched together by SB ####

resource "aws_vpc" "main" {
  cidr_block = var.vpc_subnet_cidr

  tags = {
    Project = var.project_tag
  }
}


resource "aws_default_security_group" "vpc_default_sg" {
  vpc_id = aws_vpc.main.id
}


####### SUBNETS ########
resource "aws_subnet" "public" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.vpc_subnet_cidr
  availability_zone = var.subnet_az

  tags = {
    Projet = var.project_tag
    Name   = join(" ", [var.project_tag, "subnet"])
  }
}

####### ROUTING  ########

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Project = var.project_tag
    Name    = join(" ", [var.project_tag, "internet gateway"])
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Project = var.project_tag
    Name    = join(" ", [var.project_tag, "route table"])
  }

}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}

#### EIP #####

resource "aws_eip" "eip" {
  vpc = true

  tags = {
    Project = var.project_tag
  }
}
