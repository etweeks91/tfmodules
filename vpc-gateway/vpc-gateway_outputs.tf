###### OUTPUTS VPC_GATEWAY ######

#will output a value once it's been run.
output "eip_id" {
  description = "The ID of the elastic IP"
  value       = aws_eip.eip.id
}

output "public_ip" {
  value       = aws_eip.eip.public_ip
  description = "The public EIP of VPC"
}

output "vpc_id" {
  value       = aws_vpc.main.id
  description = "The VPC id"
}

output "vpc_cidr" {
  value       = aws_vpc.main.cidr_block
  description = "The VPC CIDR"
}

output "subnet_id" {
  value       = aws_subnet.public.id
  description = "The vpc subnet id"
}

output "route_table_id" {
  value       = aws_route_table.public.id
  description = "The VPC route table id"
}
