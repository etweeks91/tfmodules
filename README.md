# tfmodules

Modules for Terraform. Modules in this library can be refernced from a Terraform root module using the format:
```
source = "gitlab.com/byarbrough/tfmodules//<module>?ref=<tag>"
```

## Format
The layout of this library is:
```
tfmodules/
├── module1
├── module2
└── module3
    ├── main.tf
    ├── outputs.tf
    ├── var.tf
    └── README.md
```
